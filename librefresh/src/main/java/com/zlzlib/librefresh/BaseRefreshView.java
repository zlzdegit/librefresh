package com.zlzlib.librefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


/**
 * 头部和底部的view的基本类
 *
 * @DateTime: 2020/8/5 10:46
 * @Author zlz
 * @Version 1.0
 */
public abstract class BaseRefreshView extends FrameLayout implements RefreshViewListener {

    private RefreshWaveView waveView;
    private boolean isShowWave = true;
    private int waveColor;

    public BaseRefreshView(@NonNull Context context) {
        this(context, null);
    }

    public BaseRefreshView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseRefreshView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void setProgressValue(int value) {

    }

    public void setShowWave(boolean showWave) {
        isShowWave = showWave;
    }

    public void setWaveColor(int waveColor) {
        this.waveColor = waveColor;
        if (null != waveView) {
            waveView.setColor(this.waveColor);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (isShowWave) {
            waveView = new RefreshWaveView(getContext());
            waveView.setColor(waveColor);
            addView(waveView);
        }
    }


    @Override
    public void onComplete(BaseRefreshLayout refreshLayout) {
        if (waveView != null) waveView.onComplete(refreshLayout);
    }

    @Override
    public void onBegin(BaseRefreshLayout refreshLayout) {
        if (waveView != null) waveView.onBegin(refreshLayout);
    }

    @Override
    public void onPull(BaseRefreshLayout refreshLayout, float fraction) {
        if (waveView != null) waveView.onPull(refreshLayout, fraction);
    }

    @Override
    public void onRefreshing(BaseRefreshLayout refreshLayout) {
        if (waveView != null) waveView.onRefreshing(refreshLayout);
    }

    public static float limitValue(float a, float b) {
        float valve = 0;
        final float min = Math.min(a, b);
        final float max = Math.max(a, b);
        valve = Math.max(valve, min);
        valve = Math.min(valve, max);
        return valve;
    }
}
