package com.zlzlib.librefresh;

/**
 * 刷新加载监听
 *
 * @DateTime: 2020/8/5 10:14
 * @Author zlz
 * @Version 1.0
 */
public abstract class RefreshListener {

    public void onRefreshFinish() {
    }

    public abstract void onRefresh(BaseRefreshLayout refreshLayout);

    public void onRefreshLoadMore(BaseRefreshLayout refreshLayout) {
    }

    public void onLoadMoreFinish() {
    }
}
