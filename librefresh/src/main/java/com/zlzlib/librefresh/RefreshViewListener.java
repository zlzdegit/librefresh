package com.zlzlib.librefresh;

/**
 * 加载刷新展示的控件的监听
 *
 * @DateTime: 2020/8/5 10:35
 * @Author zlz
 * @Version 1.0
 */
public interface RefreshViewListener {
    /**
     * 操作完成
     *
     * @param refreshLayout
     */
    void onComplete(BaseRefreshLayout refreshLayout);

    /**
     * 操作开始
     *
     * @param refreshLayout
     */
    void onBegin(BaseRefreshLayout refreshLayout);

    /**
     * 拉动回调
     *
     * @param refreshLayout
     * @param fraction 拉动的值
     */
    void onPull(BaseRefreshLayout refreshLayout, float fraction);

    /**
     * 正在执行刷新
     *
     * @param refreshLayout
     */
    void onRefreshing(BaseRefreshLayout refreshLayout);
}
