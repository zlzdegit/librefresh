package com.zlzlib.librefresh;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;


/**
 * 特效背景
 *
 * @DateTime: 2020/8/6 14:13
 * @Author zlz
 * @Version 1.0
 */
public class RefreshWaveView extends View implements RefreshViewListener {

    private int waveHeight;
    private int viewHeight;
    public static int DefaultWaveHeight;
    public static int DefaultViewHeight;
    private Path path;
    private Paint paint;
    private int color;

    public RefreshWaveView(Context context) {
        this(context, null, 0);
    }

    public RefreshWaveView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RefreshWaveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setWillNotDraw(false);
        path = new Path();
        paint = new Paint();
        paint.setAntiAlias(true);
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        paint.setColor(color);
    }

    public int getViewHeight() {
        return viewHeight;
    }

    public void setViewHeight(int viewHeight) {
        this.viewHeight = viewHeight;
    }

    public int getWaveHeight() {
        return waveHeight;
    }

    public void setWaveHeight(int waveHeight) {
        this.waveHeight = waveHeight;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        path.reset();
        path.lineTo(0, viewHeight);
        path.quadTo(getMeasuredWidth() / 2f, viewHeight + waveHeight, getMeasuredWidth(), viewHeight);
        path.lineTo(getMeasuredWidth(), 0);
        canvas.drawPath(path, paint);
    }


    @Override
    public void onComplete(BaseRefreshLayout br) {
        waveHeight = 0;
        ValueAnimator animator = ValueAnimator.ofInt(viewHeight, 0);
        animator.setDuration(200);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.start();
        animator.addUpdateListener(animation -> {
            viewHeight = (int) animation.getAnimatedValue();
            invalidate();
        });
    }

    @Override
    public void onBegin(BaseRefreshLayout br) {
    }

    @Override
    public void onPull(BaseRefreshLayout br, float fraction) {
        setViewHeight((int) (Utils.dip2px(getContext(), DefaultViewHeight) * BaseRefreshView.limitValue(1, fraction)));
        setWaveHeight((int) (Utils.dip2px(getContext(), DefaultWaveHeight) * Math.max(0, fraction - 1)));
        invalidate();
    }

    @Override
    public void onRefreshing(BaseRefreshLayout br) {
        setViewHeight((int) (Utils.dip2px(getContext(), DefaultViewHeight)));
        ValueAnimator animator = ValueAnimator.ofInt(getWaveHeight(), 0);
        animator.addUpdateListener(animation -> {
            setWaveHeight((int) animation.getAnimatedValue());
            invalidate();
        });
        animator.setInterpolator(new BounceInterpolator());
        animator.setDuration(200);
        animator.start();
    }

}
