package com.zlzlib.librefresh;

import android.content.Context;

/**
 * @DateTime: 2021/7/19 14:44
 * @Author zlz
 * @Version 1.0
 */
public class Utils {


    public static float getScale(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    /**
     * @return dp转成px
     */
    public static int dip2px(Context context, float dipValue) {
        return (int) (dipValue * getScale(context) + 0.5f);
    }

}
