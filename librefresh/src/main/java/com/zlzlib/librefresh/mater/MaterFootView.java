package com.zlzlib.librefresh.mater;

import android.content.Context;
import android.util.AttributeSet;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zlzlib.librefresh.BaseRefreshLayout;


/**
 * @DateTime: 2020/8/6 16:16
 * @Author zlz
 * @Version 1.0
 */
public class MaterFootView extends MaterHeadView {

    public MaterFootView(@NonNull Context context) {
        this(context, null);
    }

    public MaterFootView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaterFootView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onBegin(BaseRefreshLayout refreshLayout) {
        super.onBegin(refreshLayout);
        if (circleProgressBar != null) {
            circleProgressBar.setScaleX(1);
            circleProgressBar.setScaleY(1);
        }
    }
}
