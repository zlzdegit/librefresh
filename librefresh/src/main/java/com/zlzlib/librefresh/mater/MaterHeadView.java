package com.zlzlib.librefresh.mater;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zlzlib.librefresh.BaseRefreshLayout;
import com.zlzlib.librefresh.BaseRefreshView;
import com.zlzlib.librefresh.R;
import com.zlzlib.librefresh.Utils;


/**
 * @DateTime: 2020/8/6 14:09
 * @Author zlz
 * @Version 1.0
 */
public class MaterHeadView extends BaseRefreshView {

    protected MaterProgressBar circleProgressBar;
    protected int progressTextColor;
    protected int[] progress_colors;
    protected int progressStokeWidth = 3;
    protected boolean isShowArrow, isShowProgressBg;
    protected int progressValue, progressValueMax;
    protected int textType;
    protected int progressBg;
    protected int progressSize;

    public MaterHeadView(@NonNull Context context) {
        this(context, null);
    }

    public MaterHeadView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaterHeadView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    protected void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.MaterHeadLayout, defStyle, 0);
        int colorsId = t.getResourceId(R.styleable.MaterHeadLayout_mhl_progress_colors, R.array.material_colors);
        progress_colors = context.getResources().getIntArray(colorsId);
        isShowArrow = t.getBoolean(R.styleable.MaterHeadLayout_mhl_progress_show_arrow, true);
        textType = t.getInt(R.styleable.MaterHeadLayout_mhl_progress_text_visibility, 1);
        progressTextColor = t.getColor(R.styleable.MaterHeadLayout_mhl_progress_text_color, Color.BLACK);
        progressValue = t.getInteger(R.styleable.MaterHeadLayout_mhl_progress_value, 0);
        progressValueMax = t.getInteger(R.styleable.MaterHeadLayout_mhl_progress_max_value, 100);
        isShowProgressBg = t.getBoolean(R.styleable.MaterHeadLayout_mhl_progress_show_circle_background, true);
        progressBg = t.getColor(R.styleable.MaterHeadLayout_mhl_progress_background_color, MaterProgressBar.DEFAULT_CIRCLE_BG_LIGHT);
        int type = t.getInt(R.styleable.MaterHeadLayout_mhl_progress_size_type, 0);
        if (type == 0) {
            progressSize = 50;
        } else {
            progressSize = 60;
        }
        t.recycle();
        if (isInEditMode()) return;
        setClipToPadding(false);
        setWillNotDraw(false);
    }

    @Override
    protected void setProgressValue(int value) {
        super.setProgressValue(value);
        this.post(() -> circleProgressBar.setProgress(progressValue));

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        circleProgressBar = new MaterProgressBar(getContext());
        LayoutParams layoutParams = new LayoutParams(Utils.dip2px(getContext(), progressSize),
                Utils.dip2px(getContext(), progressSize));
        layoutParams.gravity = Gravity.CENTER;
        circleProgressBar.setLayoutParams(layoutParams);
        circleProgressBar.setColorSchemeColors(progress_colors);
        circleProgressBar.setProgressStokeWidth(progressStokeWidth);
        circleProgressBar.setShowArrow(isShowArrow);
        circleProgressBar.setShowProgressText(textType == 0);
        circleProgressBar.setTextColor(progressTextColor);
        circleProgressBar.setProgress(progressValue);
        circleProgressBar.setMax(progressValueMax);
        circleProgressBar.setCircleBackgroundEnabled(isShowProgressBg);
        circleProgressBar.setProgressBackGroundColor(progressBg);
        addView(circleProgressBar);
    }

    @Override
    public void onComplete(BaseRefreshLayout refreshLayout) {
        super.onComplete(refreshLayout);
        if (circleProgressBar != null) {
            circleProgressBar.onComplete(refreshLayout);
            circleProgressBar.setTranslationY(0);
            circleProgressBar.setScaleX(0);
            circleProgressBar.setScaleY(0);
        }
    }

    @Override
    public void onBegin(BaseRefreshLayout refreshLayout) {
        super.onBegin(refreshLayout);
        if (circleProgressBar != null) {
            circleProgressBar.onBegin(refreshLayout);
        }
    }

    @Override
    public void onPull(BaseRefreshLayout refreshLayout, float fraction) {
        super.onPull(refreshLayout, fraction);
        if (circleProgressBar != null) {
            circleProgressBar.onPull(refreshLayout, fraction);
            float a = limitValue(1, fraction);
            circleProgressBar.setScaleX(1);
            circleProgressBar.setScaleY(1);
            circleProgressBar.setAlpha(a);
        }
    }

    @Override
    public void onRefreshing(BaseRefreshLayout refreshLayout) {
        super.onRefreshing(refreshLayout);
        if (circleProgressBar != null) {
            circleProgressBar.onRefreshing(refreshLayout);
        }
    }
}
