package com.zlzlib.librefresh.mater;

import android.content.Context;
import android.util.AttributeSet;

import com.zlzlib.librefresh.BaseRefreshLayout;
import com.zlzlib.librefresh.BaseRefreshView;


/**
 * 普通progress的
 *
 * @DateTime: 2020/8/5 14:48
 * @Author zlz
 * @Version 1.0
 */
public class MaterRefreshLayout extends BaseRefreshLayout {

    public MaterRefreshLayout(Context context) {
        super(context);
    }

    public MaterRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MaterRefreshLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected BaseRefreshView initHeadView(Context context, AttributeSet attrs, int defStyleAttr) {
        return new MaterHeadView(context, attrs, defStyleAttr);
    }

    @Override
    protected BaseRefreshView initFootView(Context context, AttributeSet attrs, int defStyleAttr) {
        return new MaterFootView(context, attrs, defStyleAttr);
    }

}
