package com.zlzlib.librefresh.normal;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zlzlib.librefresh.BaseRefreshView;


/**
 * @DateTime: 2020/8/6 14:09
 * @Author zlz
 * @Version 1.0
 */
public class NormalFootView extends BaseRefreshView {

    public NormalFootView(@NonNull Context context) {
        this(context,null);
    }

    public NormalFootView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public NormalFootView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
