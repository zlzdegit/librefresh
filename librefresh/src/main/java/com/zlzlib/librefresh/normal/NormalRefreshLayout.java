package com.zlzlib.librefresh.normal;

import android.content.Context;
import android.util.AttributeSet;

import com.zlzlib.librefresh.BaseRefreshLayout;
import com.zlzlib.librefresh.BaseRefreshView;


/**
 * 普通上下提示的刷新和加载更多
 *
 * @DateTime: 2020/9/8 10:09
 * @Author zlz
 * @Version 1.0
 */
public class NormalRefreshLayout extends BaseRefreshLayout {

    public NormalRefreshLayout(Context context) {
        this(context, null);
    }

    public NormalRefreshLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NormalRefreshLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected BaseRefreshView initHeadView(Context context, AttributeSet attrs, int defStyleAttr) {
        return new NormalHeadView(context, attrs, defStyleAttr);
    }

    @Override
    protected BaseRefreshView initFootView(Context context, AttributeSet attrs, int defStyleAttr) {
        return new NormalFootView(context, attrs, defStyleAttr);
    }
}
